const express = require('express');
const router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
 
// Connect
const connection = (closure) => {
    return MongoClient.connect('mongodb://localhost:27017', (err, client) => {
        if (err) return console.log(err);
            closure(client.db('mean'));
    });
};

// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    res.status(501).json(response);
};
 
// Response handling
let response = {
    status: 200,
    data: [],
    message: null
};
 
// Get users
router.get('/users', (req, res) => {
    connection((db) => {
        db.collection('users')
            .find()
            .toArray()
            .then((users) => {
                response.data = users;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});

/*router.get('/users/:id', (req, res) => {
    connection((db) => {
        db.collection('users')
            .find().find()
            .toArray()
            .then((users) => {
                res.data = users;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});

router.post('/users', (req, res) => {
    connection((db) => {
        db.collection('users')
            .update().find()
            .toArray()
            .then((users) => {
                res.data = users;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});
router.delete('/users', (req, res) => {
    connection((db) => {
        db.collection('users')
            .delete().find()
            .toArray()
            .then((users) => {
                res.data = users;
                res.json(response);
            })
            .catch((err) => {
                sendError(err, res);
            });
    });
});
*/
module.exports = router;